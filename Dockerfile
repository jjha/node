FROM  ubuntu:latest
MAINTAINER Jitendra Jha

RUN apt-get update \
  && apt-get clean \
  && apt-get upgrade -y \
  && apt-get -y install vim wget git curl \
  && apt-get install -y apache2 \
  && /etc/init.d/apache2 restart
RUN cd /var/www/html/ \
  && rm -rf index.html \
  && touch index.html \
  && echo "Jitendra Jha" > index.html \
  && cat index.html
